var http = require("http");
var url = require("url");
var qs = require('querystring');

var reddit = {
  'hostname': 'www.reddit.com',
  'port': 80,
  'path': '',
  'user-agent': 'Readdit. A simple HTML5 Reddit Reader by wesalvaro'
};

function onRequest(request, response) {
  var reqUrl = url.parse(request.url);
  var pathname = reqUrl.pathname;
  console.log("Request for " + pathname + " received.");
  if (pathname.substr(0, 3) !== '/r/') {
    response.end();
    return;
  }
  console.log(reqUrl);
  var query = qs.parse(reqUrl.query || '');
  var callback = query.callback || null;
  console.log(query);
  reddit.path = pathname + '.json?' + reqUrl.query;
  console.log(reddit.hostname + reddit.path);
  http.get(reddit, function(res) {
    response.writeHead(200, {'Content-Type': 'text/javascript'});
    res.setEncoding('utf8');
    
    if (callback) response.write(callback + '(');
    //stream the data into the response
    res.on('data', function (chunk) {
      response.write(chunk);
    });

    //write the data at the end
    res.on('end', function(){
      if (callback) response.write(')');
      response.end();
    });
    //The connection is also auto closed
  }).on('error', function(e) {
    response.writeHead( 500 ); //Internal server error
    response.end( "Got error " + e); //End the request with this message
  });
}

module.exports = http.createServer(onRequest).listen(process.env.PORT);
console.log("Server has started.");